using System;
using System.Collections.Generic;
using Xunit;
using Mesozoic;
namespace MesozoicTest
{
    public class UnitTest1
    {
        [Fact]
        public void TestDinosaurConstructor()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Assert.Equal("Louis", louis.GetName());
            Assert.Equal("Stegausaurus",louis.GetSpecie());
            Assert.Equal(12, louis.GetAge());
        }
        [Fact]
        public void TestDinosaurRoar()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Assert.Equal("Grrr !", louis.roar());
        }
        [Fact]
        public void TesthordeayHello()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Assert.Equal("Je suis Louis le Stegausaurus, j'ai 12 ans.", louis.sayHello());
        }
        [Fact]
        public void TestDinosaurHug()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Dinosaur nessie = new Dinosaur("Nessie","Diplodocus", 11);
            Assert.Equal("Je suis Louis et je fais un calin à Nessie",louis.hug(nessie));
        }
        [Fact]
        public void TestAddDinosaur()
        {
            List<Dinosaur> horde = new List<Dinosaur>();
            Horde crew = new Horde("Crew", horde);
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Dinosaur nessie = new Dinosaur("Nessie","Diplodocus", 11);
            crew.AddDinosaur(louis);
            Assert.True(horde.Count == 1);
            crew.AddDinosaur(nessie);
            Assert.True(horde.Count == 2);
        }
        [Fact]
        public void TestRemoveDinosaur()
        {
            List<Dinosaur> horde = new List<Dinosaur>();
            Horde crew = new Horde("Crew", horde);
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Dinosaur nessie = new Dinosaur("Nessie","Diplodocus", 11);
            horde.Add(louis);
            horde.Add(nessie);
            Assert.True(horde.Count == 2);
            crew.RemoveDinosaur(louis);
            Assert.True(horde.Count == 1);
        }
        [Fact]
        public void TestAllDinosaurTalk(){
            int count = 0;
            List<Dinosaur> horde = new List<Dinosaur>();
            Horde crew = new Horde("Crew", horde);
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Dinosaur nessie = new Dinosaur("Nessie","Diplodocus", 11);
            crew.AddDinosaur(louis);
            crew.AddDinosaur(nessie);
            foreach(Dinosaur dino in horde){
                count++;
            }
            Assert.Equal(count, horde.Count);
        }
    }
}
