using System;
using System.Collections.Generic;
namespace Mesozoic 
{
    public class Dinosaur
    {
        private string name;
        private string specie;
        private int age;

        public Dinosaur(string name, string specie, int age)
        {
            this.name = name;
            this.specie = specie;
            this.age = age;
        }

        public string sayHello()
        {
            return String.Format("Je suis {0} le {1}, j'ai {2} ans.", this.name, this.specie, this.age);
        }

        public string roar()
        {
            return "Grrr !";
        }
        public string hug(Dinosaur dinosaur)
        {
            return String.Format("Je suis {0} et je fais un calin à {1}",this.name, dinosaur.name);
        }
        public void SetName(string name)
        {
            this.name = name;
        }
        public string GetName()
        {
            return this.name;
        }
        public void SetSpecie(string specie)
        {
            this.specie = specie;
        }
        public string GetSpecie()
        {
            return this.specie;
        }
        public void SetAge(int age)
        {
            this.age = age;
        }
        public int GetAge()
        {
            return this.age;
        }
    }
    public class Horde
    {
        private List<Dinosaur> membres;
        private string name;
        public string GetName(){
            return this.name;
        }
        public Horde(string name, List<Dinosaur> membres){
            this.name = name;
            this.membres = membres;
        }
        public void AddDinosaur(Dinosaur dinosaur){
            this.membres.Add(dinosaur);
        }
        public void RemoveDinosaur(Dinosaur dinosaur){
            this.membres.Remove(dinosaur);
        }
        public string AllDinosaurTalk(){
            foreach(Dinosaur dino in this.membres){
                Console.WriteLine("Je suis {0} le {1}, j'ai {2} ans.", dino.GetName(), dino.GetSpecie(), dino.GetAge());
            }
            return null;
        }
    }
}