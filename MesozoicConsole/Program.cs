﻿using System;
using System.Collections.Generic;
using Mesozoic;

namespace MesozoicConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Dinosaur> horde = new List<Dinosaur>();
            Horde gang = new Horde("Gang", horde);
            Dinosaur louis = new Dinosaur("Louis","Stegausaurus",12);
            Dinosaur nessie = new Dinosaur("Nessie","Diplodocus",11);
            Console.WriteLine(louis.sayHello());
            Console.WriteLine(nessie.sayHello());
            Console.WriteLine(louis.roar());
            Console.WriteLine(nessie.roar());
            Console.WriteLine(louis.hug(nessie));
            Console.WriteLine(nessie.hug(louis));
            gang.AddDinosaur(louis);
            gang.AddDinosaur(nessie);
            gang.AllDinosaurTalk();
            gang.RemoveDinosaur(louis);
        }
    }
}