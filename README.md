# TP5-Conception Objet Mézosoïque/Dinosaures

[![forthebadge](http://forthebadge.com/images/badges/built-with-love.svg)](http://forthebadge.com)  [![forthebadge](http://forthebadge.com/images/badges/powered-by-electricity.svg)](http://forthebadge.com)

TP5 par Samy Delahaye- Initiation aux classes en C#

## Réalisé avec

* [Visual Studio Code](https://code.visualstudio.com/) - IDE

## Auteurs

* **Hugo Benabdelhak** _alias_ [@hugo.ben](https://gitlab.com/hugo.ben)

